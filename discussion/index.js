// ES6 Updates - EcmaScript


//Exponent Operator

// old
const oldnum = Math.pow(8,2);
console.log("Result old: ");
console.log(oldnum);

// new
const newNum = 8 ** 2;
console.log("Result of ES6: ");
console.log(newNum);


// TEMPLATE LITERALS
/*
	- allows us to write strings without using the concatenate operator(+)
*/

let studentName = "Roland";

//Pre-template Literal String
console.log("Pre-template literal: ");
console.log("Hello "+ studentName + "! Welcome to programming!");

// Template Literal String
console.log("Template literal:");
console.log(`Hello ${studentName}! Welcome to programming!`);

//Multi-line template literal
const message = `
${studentName} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${newNum}.`
console.log(message);

/*
	- template litarals allow us to write string with embedded JavaScript expressions "${}" is used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings account is: ${principal * interestRate}`)


// ARRAY DESTRUCTURING
/*
	- allows us to unpack elements in arrays into distinct variables
	- allows us to name array elements with variables instead of using index number
	- syntax:
		let/const [variableName, variableName, variableName] = array;
*/
console.log("Array Destructuring: ")

const fullName = ['Jeru', 'Nebur', 'Palma']

// Pre-Array Destructuring
console.log("Pre-Array Destructuring: ")
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)
console.log("");

// Array Destructuring
const [firstName, middleName, lastName] = fullName 

console.log("Array Destructuring: ")
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

// OBJECT DESTRUCTURING
/*
	- ALLOWS US To ubpack properties on objects into distinct values
	- shortens the syntax for accessing properties from objects
	- syntax:
		let/const {propertyName, propertyName, propertyName} = object;
*/
console.log('Object Destructuring')

const person = {
	givenName: "Jane",
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// Pre Object Destructuring
console.log('Pre Object Destructuring')
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log("")

// Object Destructuring
console.log('Object Destructuring')
const { givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}`);


//ARROW FUNCTIONS
/*
	- compact alternative syntax to traditional functions
	- useful for creating codes snippets where creating functions will not be reused in any other portions of the code
	- adheres to the DRY principle (Dont repeat yourself) principle where ther is no longer need to create a new function and think of a name for functions that will only be used in certain code snippets.
*/	

const hello = () => {
	console.log("Good morning batch 241")
};
console.log(hello);
/*
	- syntax:
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log();	
		}
*/

const printFullName = (firstN, middleN, lastN) =>{
	console.log(`${firstN} ${middleN} ${lastN}`);
}
printFullName('John', 'D', 'Smith');
printFullName('Eric', 'A', 'Andales');

const students = ['John', 'Jane', 'Smith']

//old
students.forEach(function(students){
	console.log(`${students} is a student`)
})
console.log("")

//new
students.forEach((students) => {
	console.log(`${students} is a student`);
})

/*let friend = {
	talk = function(person){
	console.log(`Hey ${person}`)
	}
}*/


// Implicit Return Statement
/*
	- there are instances when you can omit the return statement
	- this works beciase even without the return statement, Javascript addas for it fior the result of the ffuntion
*/

// old/pre-arrow function
const add = (x, y) =>{
	return x + y;
}
let total = add(1, 2)
console.log(total);

// arrow function
const subtract = (x, y) => x - y
let difference = subtract (3, 1)
console.log(difference);


//Default Argument Value
/*
 - provides a default argument if non is provided when the function is invoked.
*/
console.log('DEFAULT ARGUMENT VALUE');

const greet = (name = 'User') => {
	return `Good morning, ${name}`
}
console.log(greet());
console.log(greet('Jeru'));



// Class-Based Object Blueprints
/*
	- allows crreation/instantiation of objects as blueprints

	Creating a class
	- the constructor function is a special method of class for creating/initializing an object for that class.
	- the "this" keyword refers to the properties initialized from the inside the class
	- syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
/*
 - "new" = to instantiate
 - no arguments will create an object without any values assigned to its properties
*/	
let myCar = new car();
console.log(myCar);
console.log("");

/*console.log("Console");
console.log(console);
console.log(window);*/

//Assigning properties

myCar['brand'] = "Ford";
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
console.log(myCar);


// Instantiate a new object
const myNewCar = new car("Tokyo", "Vios", "2021");
console.log(myNewCar);

